#include <QtPlugin>

#include "7zVersion.h"

#include "mainwindow.h"
#include "prototype.h"
#include "utils.h"
#include "processmanager.h"
#include "gamelist.h"
#include "screenshot.h"
#include "audit.h"
#include "mameopt.h"
#include "dialogs.h"
#include "ips.h"
#include "m1.h"

MainWindow *win;
QSettings *pGuiSettings;
QSettings defaultGuiSettings(":/res/kmame" INI_EXT, QSettings::IniFormat);
QString currentAppDir;
QString mame_binary;
QString language;
bool local_game_list;
bool isDarkBg = false;
bool sdlInited = false;
bool isMESS = false;
bool isUME = false;
QStringList validGuiSettings;

int main(int argc, char *argv[])
{

    for (int i = 1; i < argc; i++)
	{
		if (QString(argv[i]) == "-configpath" && i + 1 < argc)
		{
			CFG_PREFIX = utils->getPath(argv[i + 1]);
			break;
		}
	}

	pGuiSettings = new QSettings(CFG_PREFIX + "mamepgui" INI_EXT, QSettings::IniFormat);

	QApplication myApp(argc, argv);

	QTranslator appTranslator;

	language = pGuiSettings->value("language").toString();
	if (language.isEmpty())
		language = QLocale::system().name();
	appTranslator.load(":/lang/mamepgui_" + language);

	myApp.installTranslator(&appTranslator);

	if (language.startsWith("zh_") || language.startsWith("ja_"))
	{
		QFont font;
		font.setPointSize(9);
		myApp.setFont(font);
	}
	
	procMan = new ProcessManager(0);	
	utils = new Utils(0);
	win = new MainWindow(0);

	return myApp.exec();
}
