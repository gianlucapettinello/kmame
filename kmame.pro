include(common_settings.pri)

DESTDIR = bin
TARGET = kmame
QT += xml widgets concurrent

#INCLUDE
INCLUDEPATH += quazip lzma include include/zlib include/SDL include/SDL/$${OSDIR}

#LIBS
#TARGETDEPS += \
#	./lib/$${OSDIR}/libquazip.a \
#	./lib/$${OSDIR}/liblzma.a

#LIBS += -L./lib/$${OSDIR}
QMAKE_LIBDIR += ./lib/$${OSDIR}
LIBS += -lquazip1-qt5 -llzma -lz

build_sdl {
	macx: LIBS += -framework Cocoa -framework IOKit -framework CoreAudio -framework AudioToolbox -framework AudioUnit -framework OpenGL -framework ForceFeedback
	LIBS += -lSDL
	DEFINES += USE_SDL
}

HEADERS += \
	prototype.h \
	mainwindow.h \
	screenshot.h\
	dialogs.h \
	audit.h \
	gamelist.h \
	mameopt.h \
	utils.h \
	processmanager.h\
	ips.h \
	m1.h

SOURCES += \
	prototype.cpp \
	screenshot.cpp\
	dialogs.cpp \
	audit.cpp \
	gamelist.cpp \
	mameopt.cpp \
	utils.cpp \
	processmanager.cpp\
	ips.cpp \
	m1.cpp \
	mainwindow.cpp \
	main.cpp

FORMS += \
	mainwindow.ui \
	playoptions.ui \
	options.ui \
	csvcfg.ui \
	directories.ui \
	about.ui \
	cmd.ui \
	ips.ui \
	m1.ui

TRANSLATIONS = \
	lang/kmame_zh_CN.ts \
	lang/kmame_zh_TW.ts \
	lang/kmame_ja_JP.ts \
	lang/kmame_es_ES.ts \
	lang/kmame_fr_FR.ts \
	lang/kmame_hu_HU.ts \
	lang/kmame_ko_KR.ts \
	lang/kmame_pt_BR.ts \
	lang/kmame_ru_RU.ts \
	lang/kmame_it_IT.ts

RESOURCES = kmame.qrc

win32 {
	RC_FILE = kmame.rc
	QMAKE_LFLAGS_RELEASE += -static-libgcc
}

macx {
	ICON = kmame.icns
}
